import * as React from "react";
import * as ReactDOM from "react-dom";
import Hello from "./greetings";

ReactDOM.render(
    <Hello name="Alena" />,
    document.getElementById("root")
);
